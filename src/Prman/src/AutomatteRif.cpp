
#include "ri.h"
#include "rx.h"
#include "RifPlugin.h"
#include <stdio.h>
#include <string.h>
#include <iostream>

#include "AutomatteCollector.h"

class AutomatteRif : public RifPlugin
{
	RifFilter         m_rifFilter;
	RtToken           m_inputBxdfParam;
public:
	              	   AutomatteRif();
	virtual           ~AutomatteRif();
	virtual RifFilter & GetFilter()  { return m_rifFilter; }

	static void        RiBxdfV(RtToken name, RtToken handle, 
	                           RtInt n, RtToken nms[], RtPointer vals[]);

	static void        pixelFilter( RtFilterFunc func, 
                               RtFloat xwidth, RtFloat ywidth);

	void instantiateAutomatte();

	// Frustratingly we need to override all primitives as these will trigger
	// the AutomatteBsdf insertion point.

	static RtVoid   blobbyV(RtInt nleaf, RtInt ninst, RtInt inst[],
                    RtInt nflt, RtFloat flt[],
                    RtInt nstr, RtToken str[],
                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   coneV(RtFloat height, RtFloat radius, RtFloat tmax,
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   curvesV(RtToken type, RtInt ncurves, RtInt nvert[],
	                    RtToken wrap, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   cylinderV(RtFloat rad, RtFloat zmin, RtFloat zmax,
	                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   diskV(RtFloat height, RtFloat radius, RtFloat tmax,
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   generalPolygonV(RtInt nloops, RtInt nverts[],
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   geometryV(RtToken type, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   hyperboloidV(RtPoint point1, RtPoint point2,
	                    RtFloat tmax,
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   nuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
	                    RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder,
	                    RtFloat vknot[], RtFloat vmin, RtFloat vmax,
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   paraboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax,
	                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   patchMeshV(RtToken type, RtInt nu, RtToken uwrap,
	                    RtInt nv, RtToken vwrap,
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   patchV(RtToken type, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   pointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
	                    RtInt nverts[], RtInt verts[],
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   pointsPolygonsV(RtInt npolys, RtInt nverts[],
	                    RtInt verts[], RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   pointsV(RtInt nverts, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   polygonV(RtInt nverts, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   procedural(RtPointer data, RtBound bound,
	                    RtProcSubdivFunc sdfunc, RtProcFreeFunc freefunc);
	static RtVoid   sphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
	                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   subdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[],
	                    RtInt verts[], RtInt nt, RtToken tags[],
	                    RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
	                    RtInt np, RtToken pNames[], RtPointer pValues[]);
	static RtVoid   torusV(RtFloat majrad, RtFloat minrad,
	                    RtFloat phimin, RtFloat phimax,
	                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[]);
};

struct RiPixelFilterDelegate : AMT::PixelFilter
{
	RiPixelFilterDelegate( RtFilterFunc f, float xw, float yw ) :
		AMT::PixelFilter( xw, yw ),
		filterFunc(f)
	{}

	virtual float filter( float x, float y )
	{
		return filterFunc( x, y, this->xwidth, this->ywidth );
	}
	RtFilterFunc filterFunc;
};

AutomatteRif::AutomatteRif()
{
	m_rifFilter.ClientData  = static_cast<void *>(this);
    m_rifFilter.BxdfV       	= AutomatteRif::RiBxdfV;
    m_rifFilter.PixelFilter     = AutomatteRif::pixelFilter;
    m_rifFilter.BlobbyV 		= AutomatteRif::blobbyV;
    m_rifFilter.ConeV 			= AutomatteRif::coneV;
    m_rifFilter.CurvesV 		= AutomatteRif::curvesV;
    m_rifFilter.CylinderV 		= AutomatteRif::cylinderV;
    m_rifFilter.DiskV 			= AutomatteRif::diskV;
    m_rifFilter.GeneralPolygonV = AutomatteRif::generalPolygonV;
    m_rifFilter.GeometryV 		= AutomatteRif::geometryV;
    m_rifFilter.HyperboloidV 	= AutomatteRif::hyperboloidV;
    m_rifFilter.NuPatchV 		= AutomatteRif::nuPatchV;
    m_rifFilter.ParaboloidV 	= AutomatteRif::paraboloidV;
    m_rifFilter.PatchMeshV 		= AutomatteRif::patchMeshV;
    m_rifFilter.PatchV 			= AutomatteRif::patchV;
    m_rifFilter.PointsGeneralPolygonsV 	= AutomatteRif::pointsGeneralPolygonsV;
    m_rifFilter.PointsPolygonsV = AutomatteRif::pointsPolygonsV;
    m_rifFilter.PointsV 		= AutomatteRif::pointsV;
    m_rifFilter.PolygonV 		= AutomatteRif::polygonV;
    m_rifFilter.Procedural 		= AutomatteRif::procedural;
    m_rifFilter.SphereV 		= AutomatteRif::sphereV;
    m_rifFilter.SubdivisionMeshV = AutomatteRif::subdivisionMeshV;
    m_rifFilter.TorusV 			= AutomatteRif::torusV;
    m_rifFilter.Filtering  		= RifFilter::k_Continue;
}
AutomatteRif::~AutomatteRif() {}

RifPlugin * RifPluginManufacture(int argc, char **argv)
{
    fprintf(stdout, "Automatte Enabled! \"Leave everything to me!\"\n");
    return new AutomatteRif();
}

void AutomatteRif::RiBxdfV(
	RtToken name, RtToken handle, RtInt n, RtToken nms[], RtPointer vals[] )
{
	// Create the intended source bxdf.
	::RiBxdfV( name, handle, n, nms, vals );

	// Store the bxdf handle name so we can use it at the point of primitive
	// instatiation.
	AutomatteRif *obj = static_cast<AutomatteRif *>( RifGetCurrentPlugin() );	
	obj->m_inputBxdfParam = handle;
}

void AutomatteRif::pixelFilter( RtFilterFunc func, 
                               RtFloat xwidth, RtFloat ywidth )
{
	// Create the intended pixel filter.
	::RiPixelFilter( func, xwidth, ywidth );

	RiPixelFilterDelegate delegate( func, xwidth, ywidth );

	// Tell the global collector instance about the pixel filter we have chosen.
	AMT::AutomatteCollector * collector = AMT::AutomatteCollector::Get();
	collector->setPixelFilter( delegate );
}

void AutomatteRif::instantiateAutomatte()
{
	static RtToken kAutomatteBxdf = RifGetToken("AutomatteBxdf");
	static RtToken kInputParam    = RifGetToken("reference bxdf inputBxdf");

	if ( m_inputBxdfParam != NULL )
	{
		RtToken id_name = RifGetToken( (std::string(m_inputBxdfParam) + std::string("_automatte")).c_str() );

		RtToken paramNames[]  = { kInputParam, NULL };
		RtPointer paramVals[] = { m_inputBxdfParam, NULL };

		// Create a new AutomatteBxdf and insert the previous Bxdf as the input
		::RiBxdfV( kAutomatteBxdf, id_name, 1, paramNames, paramVals );
	}
}

#define INSTANTIATE_AUTOMATTE                                                  \
	AutomatteRif *obj = static_cast<AutomatteRif *>( RifGetCurrentPlugin() );  \
	obj->instantiateAutomatte()

// primitives ------------------------------------------------------------
RtVoid AutomatteRif::blobbyV(RtInt nleaf, RtInt ninst, RtInt inst[],
                    RtInt nflt, RtFloat flt[],
                    RtInt nstr, RtToken str[],
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiBlobbyV(nleaf,ninst,inst,nflt,flt,nstr,str,np,pNames,pValues);
}

RtVoid AutomatteRif::coneV(RtFloat height, RtFloat radius, RtFloat tmax,
                    RtInt np, RtToken pNames[], RtPointer pValues[])
{
	INSTANTIATE_AUTOMATTE;
	RiConeV( height, radius, tmax, np, pNames, pValues );
}

RtVoid AutomatteRif::curvesV(RtToken type, RtInt ncurves, RtInt nvert[],
                    RtToken wrap, RtInt np, RtToken pNames[], RtPointer pValues[] )
{
 	INSTANTIATE_AUTOMATTE;
	RiCurvesV(type, ncurves, nvert, wrap, np, pNames, pValues );
}

RtVoid AutomatteRif::cylinderV(RtFloat rad, RtFloat zmin, RtFloat zmax,
                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiCylinderV(rad, zmin, zmax, tmax, np, pNames, pValues );
}

RtVoid AutomatteRif::diskV(RtFloat height, RtFloat radius, RtFloat tmax,
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiDiskV(height, radius, tmax, np, pNames, pValues );
}

RtVoid AutomatteRif::generalPolygonV(RtInt nloops, RtInt nverts[],
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiGeneralPolygonV(nloops, nverts, np, pNames, pValues );
}

RtVoid AutomatteRif::geometryV(RtToken type, RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiGeometryV( type, np, pNames, pValues );
}

RtVoid AutomatteRif::hyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax,
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiHyperboloidV( point1, point2, tmax, np, pNames, pValues );
}

RtVoid AutomatteRif::nuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                    RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder,
                    RtFloat vknot[], RtFloat vmin, RtFloat vmax,
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiNuPatchV(nu,uorder,uknot,umin,umax,nv,vorder,vknot,vmin,vmax,np,pNames,pValues);
}

RtVoid AutomatteRif::paraboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiParaboloidV(rmax,zmin,zmax,tmax,np,pNames,pValues);
}

RtVoid AutomatteRif::patchMeshV(RtToken type, RtInt nu, RtToken uwrap,
                    RtInt nv, RtToken vwrap,
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiPatchMeshV(type, nu, uwrap, nv, vwrap, np, pNames, pValues );

}

RtVoid AutomatteRif::patchV(RtToken type, RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiPatchV(type, np, pNames, pValues );
}

RtVoid AutomatteRif::pointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
                    RtInt nverts[], RtInt verts[],
                    RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiPointsGeneralPolygonsV(npolys, nloops, nverts, verts, np, pNames, pValues );
}

RtVoid AutomatteRif::pointsPolygonsV(RtInt npolys, RtInt nverts[],
                    RtInt verts[], RtInt np, RtToken pNames[], RtPointer pValues[] )
{
	INSTANTIATE_AUTOMATTE;
	RiPointsPolygonsV(npolys, nverts,verts, np, pNames, pValues );
}

RtVoid AutomatteRif::pointsV(RtInt nverts, RtInt np, RtToken pNames[], RtPointer pValues[])
{
	INSTANTIATE_AUTOMATTE;
	RiPointsV(nverts, np, pNames, pValues);
}

RtVoid AutomatteRif::polygonV(RtInt nverts, RtInt np, RtToken pNames[], RtPointer pValues[])
{
	INSTANTIATE_AUTOMATTE;
	RiPolygonV(nverts, np, pNames, pValues);
}

RtVoid AutomatteRif::procedural(RtPointer data, RtBound bound,
                    RtProcSubdivFunc sdfunc, RtProcFreeFunc freefunc)
{
	INSTANTIATE_AUTOMATTE;
	RiProcedural(data, bound, sdfunc, freefunc);
}

RtVoid AutomatteRif::sphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[])
{
	INSTANTIATE_AUTOMATTE;
	RiSphereV( radius, zmin, zmax, tmax, np, pNames, pValues );
}

RtVoid AutomatteRif::subdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[],
                    RtInt verts[], RtInt nt, RtToken tags[],
                    RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
                    RtInt np, RtToken pNames[], RtPointer pValues[])
{
	INSTANTIATE_AUTOMATTE;
	RiSubdivisionMeshV(mask, nf, nverts,verts, nt, tags,nargs, intargs, floatargs,
                    	np, pNames, pValues);
}

RtVoid AutomatteRif::torusV(RtFloat majrad, RtFloat minrad,
                    RtFloat phimin, RtFloat phimax,
                    RtFloat tmax, RtInt np, RtToken pNames[], RtPointer pValues[])
{
	INSTANTIATE_AUTOMATTE;
	RiTorusV(majrad, minrad, phimin, phimax, tmax, np, pNames, pValues);
}
