#ifndef AUTOMATTERICOMMON_H
#define AUTOMATTERICOMMON_H

#include "ri.h"
#include "rx.h"
#include "AutomatteCollector.h"

struct PrmanPixelFilter : AMT::PixelFilter
{
	RtFilterFunc prmanFilter;

	PrmanPixelFilter( float xw, float yw, RtFilterFunc fn ) : 
		AMT::PixelFilter(xw,yw), prmanFilter(fn) {} 

	inline virtual float filter( float x, float y )
	{
		return prmanFilter( x, y, this->xwidth, this->ywidth );
	}
};

inline void InitialiseCollectorFromRiStream( AMT::AutomatteCollector & collector )
{
	RxInfoType_t rtype;
	int          rcount;

	float format[3] = { 640.0f, 480.0f, 1.0f };
	RxOption("Ri:Format", &format, sizeof(format), &rtype, &rcount );
	assert( rtype == RxInfoFloat );
	assert( rcount == 3 );

	float frameAspectRatio = 1.0f;
	RxOption("Ri:FrameAspectRatio", &frameAspectRatio, sizeof(float), &rtype, &rcount );
	assert( rtype == RxInfoFloat );
	assert( rcount == 1 );

	float cropWindow[4];
	RxOption("Ri:CropWindow", cropWindow, sizeof(float[4]), &rtype, &rcount );
	assert( rtype == RxInfoFloat );
	assert( rcount == 4 );

	float screenWindow[4] = {0.0f,0.0f,1.0f,1.0f};
	RxOption("Ri:ScreenWindow", screenWindow, sizeof(float[4]), &rtype, &rcount );
	assert( rtype == RxInfoFloat );
	assert( rcount == 4 );

	int tileSize[2] = {16, 16};
	RxOption("limits:bucketsize", tileSize, sizeof(int[2]), &rtype, &rcount );
	assert( rtype == RxInfoInteger );
	assert( rcount == 2 );

	PrmanPixelFilter * pixelFilter = new PrmanPixelFilter( 2, 2, RiGaussianFilter );

	// initialise the render frame geometry
	AMT::RenderFrameGeometry frame(
		format[0], format[1], format[2], frameAspectRatio,
		screenWindow, 
		cropWindow,
		tileSize,
		static_cast< AMT::PixelFilter * >( pixelFilter )
	);

	collector.initialise( frame );
}

#endif
