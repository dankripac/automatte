
#include <cmath>
#include <algorithm>
#include <sstream>
#include <string.h>

#include "AutomatteCollector.h"
#include "AutomatteRiCommon.h"

#include "rx.h"
#include "RixShading.h"
#include "RixBxdf.h"

static const unsigned char k_relflectLobeId = 0;
static const unsigned char k_refractLobeId = 0;

static RixBXLobeSampled s_relflectLobe;
static RixBXLobeSampled s_refractLobe;

static RixBXLobeTraits s_relflectLobeTraits;
static RixBXLobeTraits s_refractLobeTraits;

class AutomatteBxdf : public RixBsdf 
{
public:
	RixBsdf                 * m_passThroughBxdf; 
	AMT::AutomatteCollector * m_collector;
	AMT::AttrSetID            m_attrSetID; ///< the collapsed AttrSetID for this Bxdf instance

	AutomatteBxdf(
		RixShadingContext const * sc, 
		RixBxdfFactory * bx,
		RixBsdf * passThroughBxdf,
		AMT::AutomatteCollector * collector,
		AMT::AttrSetID            attrSetID
	) 
	:
		RixBsdf(sc,bx),
		m_passThroughBxdf(passThroughBxdf),
		m_collector(collector),
		m_attrSetID(attrSetID)
	{}


	void AccumulateIdSamples( RixBXLobeTraits const * lobesWanted );
			   
	virtual RixBXEvaluateDomain GetEvaluateDomain() {
		return m_passThroughBxdf ? 
					  m_passThroughBxdf->GetEvaluateDomain() : k_RixBXBoth;
	}

	virtual void GetAggregateLobeTraits( RixBXLobeTraits * tr ) {
		if ( m_passThroughBxdf )
			m_passThroughBxdf->GetAggregateLobeTraits(tr);
	}

	virtual void GenerateSample(
		RixBXTransportTrait transportTrait,
		RixBXLobeTraits const *lobesWanted,
		RixRNG *rng,
		RixBXLobeSampled *lobeSampled,
		RtVector3 *On, 
		RixBXLobeWeights &W,
		RtFloat *FPdf, RtFloat *RPdf
	) {
		if ( m_passThroughBxdf )
		{
			m_passThroughBxdf->GenerateSample(
				transportTrait, 
				lobesWanted, 
				rng, 
				lobeSampled,
				On,
				W,
				FPdf,
				RPdf
			);
		}

		AccumulateIdSamples( lobesWanted );
	}

	virtual void EvaluateSample(
		RixBXTransportTrait transportTrait,
		RixBXLobeTraits const *lobesWanted,
		RixBXLobeTraits *lobesEvaluated,
		RtVector3 const *On,  
		RixBXLobeWeights &W,
		RtFloat *ForwardPdf, 
		RtFloat *ReversePdf
	)
	{
		if ( m_passThroughBxdf )
		{
			m_passThroughBxdf->EvaluateSample(
				transportTrait,
				lobesWanted,
				lobesEvaluated,
				On,
				W,
				ForwardPdf,
				ReversePdf
			);
		}
	}

	virtual void EvaluateSamplesAtIndex(
		RixBXTransportTrait transportTrait,
		RixBXLobeTraits const &lobesWanted, 
		RtInt index, 
		RtInt nSamples,
		RixBXLobeTraits * lobesEvaluated,
		RtVector3 const * On, 
		RixBXLobeWeights & W,
		RtFloat * FPdf, 
		RtFloat * RPdf
	) 
	{
		if ( m_passThroughBxdf )
		{
			m_passThroughBxdf->EvaluateSamplesAtIndex(
				transportTrait,
				lobesWanted,
				index,
				nSamples,
				lobesEvaluated,
				On,
				W,
				FPdf,
				RPdf
			);
		}
	}

	virtual bool EmitLocal(RtColorRGB* result);
};


class AutomatteBxdfFactory : public RixBxdfFactory
{
public:
	/// This will default to "limits:bucketsize"
	int  m_tileSizeX;
	int  m_tileSizeY;
	
	/// use an RiAttribute to determine if mattes are collected via indirect rays
	bool m_collectReflectedSamples;
	bool m_collectTransmittedSamples;

	AMT::AutomatteCollector * m_collector;
	AMT::AttrSetID            m_attrSetID;

	AutomatteBxdfFactory() {}
	~AutomatteBxdfFactory() {}

	virtual int Init( RixContext &, const char* ) { return 0; }
	
	RixSCParamInfo const * GetParamTable() 
	{
		static RixSCParamInfo ptable[] = {
			RixSCParamInfo("inputBxdf", k_RixSCBxdf, k_RixSCInput, 0), 
			RixSCParamInfo()
		};
		return ptable;
	}

	virtual void Finalize(RixContext &) {}

	virtual void Synchronize(
		RixContext & ctx, 
		RixSCSyncMsg syncMsg,
		RixParameterList const * params
	) 
	{
		if (syncMsg == k_RixSCRenderBegin)
		{
			s_relflectLobe = RixBXLookupLobeByName(ctx, false, false, true, 
			                                      k_relflectLobeId, 
			                                      "Specular");
			s_refractLobe  = RixBXLookupLobeByName(ctx, false, false, false, 
			                                      k_refractLobeId, 
			                                      "Specular");

			s_relflectLobeTraits = RixBXLobeTraits(s_relflectLobe);
			s_refractLobeTraits = RixBXLobeTraits(s_refractLobe);

			RxInfoType_t rtype;
			int          rcount;

			RtString attribute_list[32];
			if ( 0==RxAttribute( "user:AutoMatteAttributeList", 
								attribute_list,  32*sizeof(RtString), 
								&rtype, &rcount ) && rcount > 0)
			{
				assert( rtype == RxInfoStringV );
				attribute_list[rcount] = NULL;
			} else 
			{
				// By default we look at the object name
				static const char * name = "identifier:name";
				attribute_list[0] = const_cast<RtString>( name );
				attribute_list[1] = NULL;
			}

			// Get the global instance. This should be initialised already.
			m_collector = AMT::AutomatteCollector::Get();

			// This should not really happen if we are correctly using the Rif filter.
			if ( ! m_collector->isInitialised() )
			{
				InitialiseCollectorFromRiStream( *m_collector );
			}

			// Query attributes in list and accumulate key value ids in a stack array.
			AMT::KeyValueID keyValues[rcount+1];

			int kvCount = 0;
			for ( int i=0; i<rcount; i++ )
			{
				RtString attrValue = NULL;
				if ( 0==RxAttribute( attribute_list[i], attrValue, sizeof(attrValue),
																&rtype, &rcount ) )
				{
					keyValues[kvCount] = 
						m_collector->getKeyValueId( attribute_list[i], attrValue );
					kvCount++;
				} else {
					/// Warning go here
				}
			}

			// sort key values in ascending order.
			std::sort( &keyValues[0], &keyValues[kvCount] );

			// Get a single unique ID that represents the whole set of key value pairs.
			m_attrSetID = m_collector->getAttrSetID( keyValues, kvCount );
		}
	}

	virtual RixBsdf *BeginScatter(
		RixShadingContext const * sCtx,
		RixBXLobeTraits const & lobesWanted,
		RixSCShadingMode sm,
		RtConstPointer instanceData
	) 
	{
		RixShadingContext::Allocator pool(sCtx);

		RixSCType paramType;
		RixSCConnectionInfo connectInfo;
		int arrayLen = 0;
		sCtx->GetParamInfo( 0, &paramType, &connectInfo, &arrayLen );
	
		RixBsdf * bsdf = NULL;
		if (paramType == k_RixSCBxdf) 
		{
			RixBxdfFactory* bxdfFactory = NULL;
			sCtx->EvalParam( 0, 0, &bxdfFactory );
			if ( bxdfFactory ) {
				/// Pass through to the input bxdf factory.
				bsdf = bxdfFactory->BeginScatter(sCtx,lobesWanted,sm,instanceData);
			}
		}

		AutomatteBxdf * amb = pool.AllocForBxdf<AutomatteBxdf>(1);

		new (amb) AutomatteBxdf(sCtx,this,bsdf,
			                    m_collector,
			                    m_attrSetID
			                    );

		return amb;
	}

	virtual void EndScatter( RixBsdf * bsdf )
	{
		AutomatteBxdf * amb = dynamic_cast<AutomatteBxdf*>( bsdf );
		if ( amb && amb->m_passThroughBxdf ) 
		{
			RixBxdfFactory * f = amb->m_passThroughBxdf->GetBxdfFactory();
			f->EndScatter( amb->m_passThroughBxdf );
		}
	}
};

void AutomatteBxdf::AccumulateIdSamples( RixBXLobeTraits const *lobesWanted )
{
	const RtInt nPts = shadingCtx->numPts;
	const RixShadingContext * sCtx = GetShadingCtx();

	const AutomatteBxdfFactory * factory = dynamic_cast<const AutomatteBxdfFactory*>( GetBxdfFactory() );

	bool collectReflected   = factory->m_collectReflectedSamples;
	bool collectTransmitted = factory->m_collectTransmittedSamples;

	AMT::FilterLocation * filterLocation = NULL;

	static const float ONE_THIRD = 1.0f / 3.0f;

	const RtPoint3 * P;
	shadingCtx->GetBuiltinVar( RixShadingContext::k_P, &P );

	const RtFloat3 * Oi;
	shadingCtx->GetBuiltinVar( RixShadingContext::k_Oi, &Oi );
	
	/// Handle Primary camera rays
	if ( sCtx->scTraits.primaryHit )
	{
		/// Determine the raster space XYZ coordinates of this sample.
		RtPoint3 * rasterP = (RtPoint3*)RixAlloca( sizeof(RtPoint3) * nPts );

		std::memcpy( rasterP, P, sizeof(RtPoint3) * nPts );

		// Transform P on primary ray hits into raster space
		shadingCtx->Transform( RixShadingContext::k_AsPoints, 
								"current", "raster", rasterP );

		// Create a AMT::FilterLocation for each primary ay.
		for ( int i=0; i<nPts; i++ )
		{
			/// The integratorCtxIndex is analogous to a primary ray id in RIS.
			AMT::RayPathID primaryRayId = sCtx->integratorCtxIndex[i];
			AMT::SubPixel rasterCoord = { rasterP[i][0], rasterP[i][1] };
			float zdepth = P[i][2];

			/// We need to create a FilterLocation for each sample and track 
			/// the seconday samples via the primary ray ID.
			filterLocation = m_collector->createFilterLocation( 
                                               primaryRayId, 
                                               rasterCoord,
                                               m_attrSetID
		                                    );

			// This needs to be the opactiy
			float weight = (Oi[i][0] + Oi[i][1] + Oi[i][2]) * ONE_THIRD;

			// accumulate the primary coverage sample
			m_collector->collectSample(
				filterLocation, 
				m_attrSetID,
				AMT::kRayType_Primary,
				zdepth,
				weight
			);
		}
	} else
	if ( collectReflected || collectTransmitted ) 
	{
		// Seconday rays
		for ( int i=0; i<nPts; i++ )
		{
			// tag between transmissive and reflective light.
			AMT::RayType rayType = lobesWanted[i].GetTransmit() ? 
									AMT::kRayType_Transmission :
									AMT::kRayType_Reflection;

			if ( (rayType == AMT::kRayType_Reflection   && ! collectReflected) ||
				 (rayType == AMT::kRayType_Transmission && ! collectTransmitted) )
				continue;

			AMT::RayPathID primaryRayId = sCtx->rayId[i];

			// FIXME: Not sure what to do about zdepth in secondary rays just yet....
			float zdepth = P[i][2];

			filterLocation = m_collector->getPrimaryFilterLocation( primaryRayId );

			// This needs to be the opactiy (possibly also attenuated by the BSDF itself?)
			float weight = (Oi[i][0] + Oi[i][1] + Oi[i][2]) * ONE_THIRD;

			// accumulate the secondary coverage sample
			m_collector->collectSample(
				filterLocation, 
				m_attrSetID,
				rayType,
				zdepth,
				weight
			);
		}
	}
}

