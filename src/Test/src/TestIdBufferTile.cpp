

#include "AMTTile.h"

#include <iostream>

#include <gtest/gtest.h>

TEST( IdBufferTileTest, CreateEmpty )
{
	AMT::IdBufferTile * tile = new AMT::IdBufferTile();
	ASSERT_EQ(tile->id_asUint8, static_cast<uint8_t*>( NULL) );
	delete tile;
}

class IdBufferTileTest_8bit_16PxSqr_3PxFilter : public ::testing::Test 
{
protected:
	virtual void SetUp() {
		m_tile.init(
			0, 0, 16, 16, 3, 3,
		    /* initialIdCapacity */ 4,
		    /* initialDataType */ AMT::IdBufferTile::kUint8,
		    /* channels */        AMT::IdBufferTile::kPrimary 
		);
	}

	virtual void TearDown() {
		m_tile.finalise();
	}

	AMT::IdBufferTile m_tile;
};

TEST_F( IdBufferTileTest_8bit_16PxSqr_3PxFilter, CreateCheckDestroy )
{
	ASSERT_EQ(m_tile.startPx,0);
	ASSERT_EQ(m_tile.startPy,0);
	ASSERT_EQ(m_tile.endPx,16);
	ASSERT_EQ(m_tile.endPy,16);
	ASSERT_EQ(m_tile.borderX,3);
	ASSERT_EQ(m_tile.borderY,3);
	ASSERT_EQ(m_tile.height(),22);
	ASSERT_EQ(m_tile.width(),22);
	ASSERT_EQ(m_tile.idCapacity,4);
	ASSERT_NE(m_tile.id_asUint8,static_cast<uint8_t*>( NULL ) );
	ASSERT_NE(m_tile.coverage,static_cast<float*>( NULL ) );
	std::cout << "tile memory = " << m_tile.memory() << " bytes\n";
}

TEST_F( IdBufferTileTest_8bit_16PxSqr_3PxFilter, GetSetFirstLayer )
{
	float * coverage = NULL;
	AMT::AttrSetID id = 1;
	AMT::SubPixel px = { -3.0f, -3.0f };

	uint32_t subpixelLayer = m_tile.getSubPixelLayerIndex( px, 0 );
	ASSERT_EQ(subpixelLayer,0);

	id = 64;
	m_tile.setId( subpixelLayer,   id );
	ASSERT_EQ( m_tile.getId( subpixelLayer ), id );
	coverage = m_tile.getCoverage( subpixelLayer, AMT::IdBufferTile::kPrimary, id );
	ASSERT_NE( coverage, static_cast<float*>( NULL ) );
	coverage[0] = 0.5f;

	id = 25;
	m_tile.setId( subpixelLayer+1, id );
	ASSERT_EQ( m_tile.getId( subpixelLayer+1 ), id );
	coverage = m_tile.getCoverage( subpixelLayer, AMT::IdBufferTile::kPrimary, id );
	ASSERT_NE( coverage, static_cast<float*>( NULL ) );

	id = 76;
	m_tile.setId( subpixelLayer+2, id );
	ASSERT_EQ( m_tile.getId( subpixelLayer+2 ), id );
	coverage = m_tile.getCoverage( subpixelLayer, AMT::IdBufferTile::kPrimary, id );
	ASSERT_NE( coverage, static_cast<float*>( NULL ) );

	id = 180;
	m_tile.setId( subpixelLayer+3, id );
	ASSERT_EQ( m_tile.getId( subpixelLayer+3 ), id );
	coverage = m_tile.getCoverage( subpixelLayer, AMT::IdBufferTile::kPrimary, id );
	ASSERT_NE( coverage, static_cast<float*>( NULL ) );
}

TEST_F( IdBufferTileTest_8bit_16PxSqr_3PxFilter, GetSetLastLayer )
{
	AMT::SubPixel px = { 18.0f, 18.0f };
	uint32_t subpixelLayer = m_tile.getSubPixelLayerIndex( px, 0 );
	ASSERT_EQ(subpixelLayer, (m_tile.width() * m_tile.height() * m_tile.idCapacity) - m_tile.idCapacity );
	m_tile.setId( subpixelLayer,   1 );
	ASSERT_EQ( m_tile.getId( subpixelLayer ), 1 );
	m_tile.setId( subpixelLayer+1, 2 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+1 ), 2 );
	m_tile.setId( subpixelLayer+2, 3 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+2 ), 3 );
	m_tile.setId( subpixelLayer+3, 4 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+3 ), 4 );
}

class IdBufferTileTest_16bit_16PxSqr_3PxFilter : public ::testing::Test 
{
protected:
	virtual void SetUp() {
		m_tile.init(
			0, 0, 16, 16, 3, 3,
		    /* initialIdCapacity */ 4,
		    /* initialDataType */ AMT::IdBufferTile::kUint16,
		    /* channels */        AMT::IdBufferTile::kPrimary 
		);
	}

	virtual void TearDown() {
		m_tile.finalise();
	}

	AMT::IdBufferTile m_tile;
};

TEST_F( IdBufferTileTest_16bit_16PxSqr_3PxFilter, CreateCheckDestroy )
{
	ASSERT_EQ(m_tile.startPx,0);
	ASSERT_EQ(m_tile.startPy,0);
	ASSERT_EQ(m_tile.endPx,16);
	ASSERT_EQ(m_tile.endPy,16);
	ASSERT_EQ(m_tile.borderX,3);
	ASSERT_EQ(m_tile.borderY,3);
	ASSERT_EQ(m_tile.height(),22);
	ASSERT_EQ(m_tile.width(),22);
	ASSERT_EQ(m_tile.idCapacity,4);
	ASSERT_NE(m_tile.id_asUint8,static_cast<uint8_t*>( NULL ) );
	ASSERT_NE(m_tile.coverage,static_cast<float*>( NULL ) );
}

TEST_F( IdBufferTileTest_16bit_16PxSqr_3PxFilter, GetSetFirstLayer )
{
	AMT::SubPixel px = { -3.0f, -3.0f };
	uint32_t subpixelLayer = m_tile.getSubPixelLayerIndex( px, 0 );
	ASSERT_EQ(subpixelLayer,0);
	m_tile.setId( subpixelLayer,   1 );
	ASSERT_EQ( m_tile.getId( subpixelLayer ), 1 );
	m_tile.setId( subpixelLayer+1, 2 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+1 ), 2 );
	m_tile.setId( subpixelLayer+2, 3 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+2 ), 3 );
	m_tile.setId( subpixelLayer+3, 4 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+3 ), 4 );
}

TEST_F( IdBufferTileTest_16bit_16PxSqr_3PxFilter, GetSetLastLayer )
{
	AMT::SubPixel px = { 18.0f, 18.0f };
	uint32_t subpixelLayer = m_tile.getSubPixelLayerIndex( px, 0 );
	ASSERT_EQ(subpixelLayer, (m_tile.width() * m_tile.height() * m_tile.idCapacity) - m_tile.idCapacity );
	m_tile.setId( subpixelLayer,   1 );
	ASSERT_EQ( m_tile.getId( subpixelLayer ), 1 );
	m_tile.setId( subpixelLayer+1, 2 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+1 ), 2 );
	m_tile.setId( subpixelLayer+2, 3 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+2 ), 3 );
	m_tile.setId( subpixelLayer+3, 4 );
	ASSERT_EQ( m_tile.getId( subpixelLayer+3 ), 4 );
}
