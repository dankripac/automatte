#ifndef AMTCOMMON_H
#define AMTCOMMON_H

#include <string>
#include <vector>
#include <assert.h>
#include <pthread.h>

#if defined(AMT_USE_TR1)
    #define STD_NS std::tr1
    #include <tr1/unordered_set>
    #include <tr1/unordered_map>
    using STD_NS::unordered_set;
    using STD_NS::unordered_map;
    #define STD_TR1_NS_BEGIN namespace tr1 {
    #define STD_TR1_NS_END   }
#else
    #define STD_NS std
    #include <unordered_set>
    #include <unordered_map>
    using STD_NS::unordered_set;
    using STD_NS::unordered_map;
    #define STD_TR1_NS_BEGIN 
    #define STD_TR1_NS_END  
#endif

#define AUTOMATTE_NS AMT
#define AUTOMATTE_NS_BEGIN namespace AUTOMATTE_NS {
#define AUTOMATTE_NS_END }

AUTOMATTE_NS_BEGIN

typedef pthread_mutex_t Mutex;

/// A RAII helper class that will lock and unlock a mutex in a C++ scope.
struct Lock
{
    Mutex * mutex;
    Lock( Mutex * m ) : mutex(m) { pthread_mutex_lock(mutex); }
    ~Lock() { pthread_mutex_unlock(mutex); }
};


/// A ray sample type used to classify sample weights
enum RayType {
    kRayType_Primary,
    kRayType_Reflection,
    kRayType_Transmission
};


/// A lookup key that represents a specific key value index pair.
struct KeyValueID
{
    int32_t attrNameIndex;
    int32_t attrValueIndex;

    /// A less-than operator used for sorting.
    inline bool operator <  ( const KeyValueID & o ) const {
    	return attrNameIndex < o.attrNameIndex ? 
    		true :
    		attrNameIndex == o.attrNameIndex ? 
    			attrValueIndex < o.attrValueIndex : false;
    }

    inline bool operator != ( const KeyValueID & o ) const {
    	// Perform as a single int64 compare.
    	return *reinterpret_cast<const uint64_t*>( this ) !=
    	       *reinterpret_cast<const uint64_t*>( &o );
    }
};

/// An integer id type that uniquely identifies a set of key value pairs
typedef uint32_t AttrSetID;


/// An integer id that uniquely identifies a ray path.
typedef uint32_t RayPathID;


/// A primary ray hit location where we can filter direct and indirect samples
struct FilterLocation;


/// A 2d vector used for raster sub-pixel coordinates
struct SubPixel 
{
    float x;
    float y;
};


/// A structure for storing per-id sample camera depth ranges
struct Depth {
    float minDepth;
    float maxDepth;
    Depth() : 
        minDepth( std::numeric_limits<float>::max() ), 
        maxDepth( std::numeric_limits<float>::min() )
    {} 
};

/// Experimental all-in-one IdPixelSample
struct IdPixelSample {
    AttrSetID id;
    float     minDepth;
    float     maxDepth;
    float     coverage;
    uint8_t   leftLayer, rightLayer, upLayer, downLayer;
    IdPixelSample() : 
        minDepth( std::numeric_limits<float>::max() ), 
        maxDepth( std::numeric_limits<float>::min() )
    {} 
};


/// A pixel filter interface to be provided by each renderer.
struct PixelFilter
{
	float xwidth;
	float ywidth;

	PixelFilter( float xw, float yw ) : xwidth(xw), ywidth(yw) {}

	/// Apply the filter function at the pixel relative xy coordinate.
	virtual float filter( float x, float y ) const;
};


AUTOMATTE_NS_END

#endif