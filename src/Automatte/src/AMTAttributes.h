#ifndef AMTATTRIBUTES_H
#define AMTATTRIBUTES_H

#include "AMTCommon.h"


AUTOMATTE_NS_BEGIN

/// A string value for an ObjectAttribute
struct AttributeValue
{
	std::string value;
	uint32_t    id;

	inline bool operator == ( const AttributeValue & other ) const {
		return value == other.value;
	}
};

AUTOMATTE_NS_END

namespace std {
STD_TR1_NS_BEGIN
	template <>
	struct hash< AMT::AttributeValue > {
		std::size_t operator ()( const AMT::AttributeValue & o ) const {
			return hash< std::string >()( o.value );
		}
	};
STD_TR1_NS_END
}

AUTOMATTE_NS_BEGIN

/// A set of named values attached to objects that we are interested in.
///
/// There will only ever be one value from ObjectAttribute::values assigned
/// to an object that is being sampled at any one time.
/// Example ObjectAttribute are:
///  * objectPath   - the path hierarchy string for a specific object.
///  * assetId      - the publishing system id for the specific object
///  * materialName - the current shader name
///  * lookId       - the publishing system id for the current material container.
struct ObjectAttribute
{
	/// The name identifier of the attribute we are interested in.
	std::string                name;

	/// The index of this ObjectAttribute
	int32_t                    id;

	/// The values of this type of attribute that have been collected while shading
	unordered_set< AttributeValue > values;

	inline bool operator == ( const ObjectAttribute & other ) const {
		return id == other.id;
	}
};

AUTOMATTE_NS_END

namespace std {
STD_TR1_NS_BEGIN
	template <>
	struct hash< AMT::ObjectAttribute > {
		std::size_t operator ()( const AMT::ObjectAttribute & o ) const {
			return STD_NS::hash<int32_t>()( o.id );
		}
	};
STD_TR1_NS_END
}

AUTOMATTE_NS_BEGIN


/// A unique set of KeyValueID that are attached to object samples. 
struct ObjectAttributeSet
{
	/// An set of KeyValueID entries that represent a specific combination
	/// of ObjectAttribute values. i.e
	/// A specific objectPath, assetId and materialName combination for example.
	std::vector< KeyValueID > keyValues;

	/// a hash of all the attribute id value indices.
	uint64_t                  hash;

	/// An integer that identifies this set that is attached to samples via IdBufferTile::setId
	AttrSetID                 id;

	inline ObjectAttributeSet( KeyValueID * kvArray, int kvCount ) : id(0)
	{
		keyValues.reserve( kvCount );
		for ( int i=0; i<kvCount; i++ )
		{
			keyValues.push_back( kvArray[i] );
		}
		hash = RecomputeHash( &*keyValues.begin(), &*keyValues.end() ); 
	}

	inline bool operator == ( const ObjectAttributeSet & other ) const {
		std::vector<KeyValueID>::const_iterator itA = keyValues.begin();
		std::vector<KeyValueID>::const_iterator itB = other.keyValues.begin();
		for ( ; itA->attrNameIndex >= 0; ++itA, ++itB ) {
			if ( *itA != *itB ) 
				return false;
		}
		return true;
	}

	inline static uint64_t RecomputeHash( KeyValueID * begin, KeyValueID * end ) 
	{
		uint64_t hash = 0;
		for ( KeyValueID * it=begin; it != end; it++ ) {
			hash ^= STD_NS::hash< uint64_t >()( reinterpret_cast<const uint64_t&>( it ) );
		}
		return hash;
	}
};

AUTOMATTE_NS_END

namespace std {
STD_TR1_NS_BEGIN
	template <>
	struct hash< AMT::ObjectAttributeSet > {
		std::size_t operator ()( const AMT::ObjectAttributeSet & o ) {
			return o.hash;
		}
	};
	template <>
	struct hash< AMT::ObjectAttributeSet * > {
		std::size_t operator ()( const AMT::ObjectAttributeSet * o ) {
			return o->hash;
		}
	};
STD_TR1_NS_END
}

AUTOMATTE_NS_BEGIN


typedef unordered_set< AttributeValue >                AttrValSet;
typedef unordered_set< AttributeValue >::iterator      AttrValSetIterator;
typedef std::vector< ObjectAttribute >                 AttrArray;
typedef std::vector< ObjectAttribute >::iterator       AttrIterator;
typedef std::vector< ObjectAttributeSet >              AttrSetArray;
typedef std::vector< ObjectAttributeSet >::iterator    AttrSetIterator;
typedef unordered_map< uint64_t, AttrSetID >           AttrSetIDMap;
typedef unordered_map< uint64_t, AttrSetID >::iterator AttrSetIDMapIterator;


AUTOMATTE_NS_END

#endif