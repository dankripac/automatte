#ifndef AMTRENDERFRAMEGEO_H
#define AMTRENDERFRAMEGEO_H

#include "AMTCommon.h"

AUTOMATTE_NS_BEGIN


/// Data that describes the geometry of a rendered image.
struct RenderFrameGeometry
{
	float         pixelResX;
	float         pixelResY;
	float         pixelAspectRatio;
	float         frameAspectRatio;
	float         screenWindow[4];
	float         cropWindow[4];
	int           tileSize[2];
	PixelFilter * pixelFilter;
	float *       pixelFilterCache; ///< Precomputed pixel filter values

	int           xStart;
	int           yStart;
	int           xEnd;
	int           yEnd;
	int           xTileCount;
	int           yTileCount;

	inline RenderFrameGeometry( 
		float _pixelResX,
		float _pixelResY,
		float _pixelAspectRatio,
		float _frameAspectRatio,
		float _screenWindow[4],
		float _cropWindow[4],
		int   _tileSize[2],
		PixelFilter * _pixelFilter
	) :
		pixelResX(_pixelResX),
		pixelResY(_pixelResY),
		pixelAspectRatio(_pixelAspectRatio),
		frameAspectRatio(_frameAspectRatio),
		pixelFilter(_pixelFilter),
		pixelFilterCache(NULL)
	{
		std::memcpy( screenWindow, _screenWindow, sizeof(float[4]) );
		std::memcpy( cropWindow,   _cropWindow,   sizeof(float[4]) );
		tileSize[0]  = std::max( 4, _tileSize[0] );
		tileSize[1]  = std::max( 4, _tileSize[1] );
		computePixelFilterCache();
	}

	RenderFrameGeometry() : pixelResX(0), pixelResY(0), 
							pixelFilter(NULL), pixelFilterCache(NULL) {}

	~RenderFrameGeometry() 
	{
		if ( pixelFilterCache ) {
			free( pixelFilterCache );
			pixelFilterCache = NULL;
		}
		if ( pixelFilter )
		{
			delete pixelFilter;
			pixelFilterCache = NULL;
		}
	}

	inline void validateTileSpace()
	{
		
	}

	/// A pre-computed pixel grid of filter attenuation values to speed up
	/// pixel filtering.
	/// For circular filters this represents a quarter ellipse
	inline void computePixelFilterCache()
	{
		if ( pixelFilterCache ) {
			free( pixelFilterCache );
			pixelFilterCache = NULL;
		}

		if ( pixelFilter )
		{
			int xwidth = std::ceil( pixelFilter->xwidth );
			int ywidth = std::ceil( pixelFilter->ywidth );
			size_t arraySize = size_t( xwidth * ywidth );

			pixelFilterCache = static_cast<float*>( malloc( arraySize * sizeof(float) ) );
			std::memset( pixelFilterCache, 0, arraySize * sizeof(float) );

			int offset = 0;
			for ( int x=0; x<xwidth; x++ )
			{
				for ( int y=0; y<ywidth; y++, offset++ )
				{
					float px = (x + 0.5f) * (pixelFilter->xwidth / (float)xwidth);
					float py = (y + 0.5f) * (pixelFilter->ywidth / (float)ywidth);
					pixelFilterCache[offset] = pixelFilter->filter(px,py);
				}
			}
		}
	}

	inline int tileIndex( SubPixel raster )
	{
		int x = std::floor(raster.x) - xStart;
		if ( x < 0 || xTileCount == 0 || yTileCount == 0 )
			return -1;

		int y = std::floor(raster.y) - yStart;
		if ( y < 0 )
			return -1;

		return ((y / yTileCount) * xTileCount) + (x / xTileCount);
	}
};

AUTOMATTE_NS_END
#endif