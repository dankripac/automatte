#ifndef AUTOMATTECOLLECTOR_H
#define AUTOMATTECOLLECTOR_H

#include "AMTTile.h"
#include "AMTRenderFrameGeometry.h"

AUTOMATTE_NS_BEGIN

/// A pixel tile grid that contains layers of AttrSetID and coverage masks
/// that are accumulated of the course of ray sampling.
class AutomatteCollector
{
public:
	/// Get the main global instance of the AutomatteCollector.
	static AutomatteCollector * Get();
	
	/// Initialise the tile grid with the render frame geometry
	void             initialise( const RenderFrameGeometry & init );

	/// Return true if initialise has been correctly called.
	bool             isInitialised() const;

	const RenderFrameGeometry & renderFrameGeometry() const;

	/// Clean up all memory and finalise usage.
	void             finalise();

	/// Return a KeyValueID which is a pair of integer ids representing the 
	/// passed key value pair.
	/// This function is thread safe.
	KeyValueID       getKeyValueId( const std::string & key, const std::string & value );

	/// Get a unique integer ID from a set of key-value pair Ids.
	/// The KeyValueID array should be pre-sorted in ascending order.
	/// This function is thread safe.
	AttrSetID        getAttrSetID( KeyValueID * attrs, int attrCount );

	/// Create a FilterLocation based on a screen sub-pixel coordinate and a primary RayPathID.
	/// @param pathId An integer id for the current ray path
	/// @param raster the raster screen coordinate
	/// @param an optional initial AttrSetId
	FilterLocation * createFilterLocation( RayPathID pathId, SubPixel raster, AttrSetID currId = 0 );

	/// Get a previously created primary FilterLocation based on a RayPathID.
	FilterLocation * getPrimaryFilterLocation( RayPathID pathId );

	/// Create or return an internal AMT::IdBufferTile for the passed screen coordinate or NULL
	/// if the tile is outside the AMT::RenderFrameGeometry that was passed to init.
	IdBufferTile * getTile( SubPixel raster );

	/// Collect an individual AttrSetID and sample weight into a FilterLocation 
	/// attenuated via the current PixelFilter and the passed weight.
	void collectSample( 
		FilterLocation * loc, ///< The location in which to filter the id coverage sample.
		AttrSetID id,         ///< The id in which to collect coverage.
		RayType rayType,      ///< the type of ray we are sampling
		float depth,          ///< a sample depth in camera space
		float weight = 1.0f   ///< An optional weight for the sample. Samples will also be
							  /// attenuated by the PixelFilter that was passed
							  /// in via AutomatteCollector::initialise
	);


	bool saveSamplesToDisk( const std::string & filepath );

private:
	AutomatteCollector();
	~AutomatteCollector();
	struct Impl;
	Impl * p_impl;
};

AUTOMATTE_NS_END

#endif
