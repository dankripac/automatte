#ifndef AMTFILTERLOCATION_H
#define AMTFILTERLOCATION_H

#include "AMTCommon.h"


AUTOMATTE_NS_BEGIN

struct IdBufferTile;

/// A primary ray hit location where we can filter direct and indirect samples
///
/// These are stored in batches locally per-thread and recycled.
struct FilterLocation
{
	/// A structure that holds pre-computed shortcuts to coverage locations 
	/// for a specific ID inside the filter tile.
	struct PixelLocation {
		float *   coverage;           ///< an array of 1-3 floats depending on if secondary rays are accumulated.
		Depth *   depth;              ///< the min max depth range for this ID sample
		float     filter;             ///< pre-calcuated pixel filter for this raster.
		uint32_t  pixelLayerIndex;    ///< the pre-computed pixel layer index for this PixelLocation
	};

	IdBufferTile *  tile;               ///< the tile buffer to filter into.
	PixelLocation * pixels;             ///< an array of prefetched PixelLocation 
	                                    /// surrounding the raster pixel coords, inside the tile.
	uint32_t        pixelCount;         ///< the number of PixelLocation allocated.
	RayPathID       rayPathId;          ///< a renderer specific primary ray identifier
	SubPixel        raster;             ///< the sub-pixel coordinates for filtering

	uint32_t        primarySampleCount; ///< The number of primary samples so far.
	uint32_t        transmissionSampleCount; ///< The number of transmission samples so far.
	uint32_t        reflectionSampleCount; ///< The number of reflection samples so far.
	AttrSetID       currentId;          ///< a mutable AttrSetID that represents the id that is currently being filtered.
	RayType         rayTraversalType;   ///< a mutable RayType that will change based on the ray traversal type.

	inline bool operator == ( const FilterLocation & other ) {
		return rayPathId == other.rayPathId;
	}
};

AUTOMATTE_NS_END

namespace std {
STD_TR1_NS_BEGIN
	template <>
	struct hash< AMT::FilterLocation > {
		std::size_t operator ()( const AMT::FilterLocation & o ) {
			// hash based on ray path id.
			return hash< AMT::RayPathID >()( o.rayPathId );
		}
	};
STD_TR1_NS_END
}

AUTOMATTE_NS_BEGIN
typedef unordered_map< RayPathID, FilterLocation * >   FilterLocactionMap;
AUTOMATTE_NS_END

#endif
