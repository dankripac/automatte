
#include "AutomatteCollector.h"

#include "AMTCommon.h"
#include "AMTAttributes.h"
#include "AMTFilterLocation.h"

#include <cmath>
#include <algorithm>
#include <sstream>
#include <limits>
#include <assert.h>

AUTOMATTE_NS_BEGIN


/// A Thread local cache
struct ThreadData
{
	/// A set of individual primary ray locations hit stored per-thread
	FilterLocactionMap  m_filterLocations;

	/// A local list of all unique named attributes values encountered while sampling.
	/// As soon as we encounter an ObjectAttribute that is not contained here we must
	/// syncronise with the central instance
	AttrArray           m_attrDefinitions;
	
	/// A local array sets of all unique combinations of ObjectAttribute found while sampling.
	/// As soon as we encounter an attrSetID that is not contained here we must
	/// syncronise with the central instance
	AttrSetArray        m_attrSets;

	/// A hash to AttrSetID lookup
	AttrSetIDMap        m_attrHashToIDMap;

	/// A collection of all ThreadData objects.
	static std::vector< ThreadData * >   s_threadData;

	static Mutex                         s_mutex;

	/// Get a thread local copy of ThreadData.
	static ThreadData * Get()
	{
		static __thread ThreadData * data = NULL;
		if ( data == NULL )
		{
			data = new ThreadData();
			Lock lock( &s_mutex );
			s_threadData.push_back( data );
		} 
		return data;
	}

	static void Finalise()
	{
		Lock lock( &s_mutex );
		std::vector< ThreadData * >::iterator it  = s_threadData.begin();
		std::vector< ThreadData * >::iterator end = s_threadData.end();
		for ( ; it != end; it++ )
		{
			delete *it;
			*it = NULL;
		}
		s_threadData.clear();
	}
};


/// The Automatte implementation and data container
struct AutomatteCollector::Impl
{
	/// An array of pointers to tiles arranged in 2d space matching the output image
	/// size.
	/// If a tile has not yet been accessed it will be NULL.
	IdBufferTile **       m_tileGrid;

	/// A central list of all unique named attributes values encountered while sampling.
	AttrArray             m_attrDefinitions;
	
	/// A central set of all unique combinations of ObjectAttribute found while sampling
	AttrSetArray          m_attrSets;

	/// A hash to id lookup2
	AttrSetIDMap          m_attrHashToIDMap;

	RenderFrameGeometry   m_frame;

	Mutex                 m_mutex;
};

AutomatteCollector::AutomatteCollector() :
	p_impl( new AutomatteCollector::Impl() )
{

}

/// Get the main global instance of the AutomatteCollector.
AutomatteCollector * AutomatteCollector::Get()
{
	static AutomatteCollector s_instance;
	return &s_instance;
}

/// Initialise the tile grid with the render frame geometry
void AutomatteCollector::initialise( const RenderFrameGeometry & init )
{
	p_impl->m_frame = init;

}

bool AutomatteCollector::isInitialised() const
{
	return p_impl->m_frame.pixelResX != 0;
}

const RenderFrameGeometry & AutomatteCollector::renderFrameGeometry() const
{
	return p_impl->m_frame;
}

/// Clean up all memory and finalise usage.
void AutomatteCollector::finalise()
{
	ThreadData::Finalise();


}

/// return an internal AMT::IdBufferTile for the passed screen coordinate or NULL
/// if the tile has not been created yet or the coordinate is outside the
/// AMT::RenderFrameGeometry that was passed to init.
IdBufferTile * AutomatteCollector::getTile( SubPixel raster )
{
	int x = std::floor( raster.x + p_impl->m_frame.cropWindow[0] );
	int y = std::floor( raster.y + p_impl->m_frame.cropWindow[1] );
	size_t index = y * int( std::floor( p_impl->m_frame.pixelResX ) ) + x;

	// TODO : make this an atomic read.
	IdBufferTile * tile = p_impl->m_tileGrid[ index ];
	if ( tile == NULL )
	{
		Lock lock( &p_impl->m_mutex );

		// double check after getting the lock.
		if ( (tile = p_impl->m_tileGrid[ index ]) != NULL )
			return tile;


		int startPx = 0;
		int startPy = 0;
		int endPx  = 16;
		int endPy  = 16;
        int borderX = 3;
        int borderY = 3;
        int initialIdCapacity = 2;
        IdBufferTile::DataType initialDataType = IdBufferTile::kUint8;
        int initChannelTypes = IdBufferTile::kPrimary;

		
		tile = new IdBufferTile;
		tile->init(
			startPx, 
			startPy, 
			endPx, 
			endPy, 
			borderX, 
			borderY,
			initialIdCapacity,
			initialDataType,
			initChannelTypes
		);

		// TODO : Make this an atomic write.
		p_impl->m_tileGrid[ index ] = tile;
	}
	return tile;
}


/// Return a KeyValueID which is a set of integer ids representing the 
/// passed key value pair.
/// This function is thread safe.
KeyValueID AutomatteCollector::getKeyValueId( const std::string & key, const std::string & value )
{
	ThreadData * threadData = ThreadData::Get();

	AttrIterator  local_attr      = threadData->m_attrDefinitions.begin();
	AttrIterator  local_attr_end  = threadData->m_attrDefinitions.end();

	AttrValSet *       local_values = NULL;
	AttrValSetIterator local_val_it;
	
	for ( ; local_attr != local_attr_end; ++local_attr )
	{
		if ( local_attr->name == key ) 
		{
			// make a test value on the stack to find in the set.
			AttributeValue test = { value, 0 };

			local_val_it = local_attr->values.find( test );
			if ( local_val_it != local_attr->values.end() )
			{
				// We have this in the local thread cache.
				KeyValueID kv = { local_attr->id, local_val_it->id };
				return kv;
			} else {
				local_values = &local_attr->values;
				break;
			}
		}
	}

	/// Not in our thread local store, we need to look in the central store.
	KeyValueID kv = { 0,0 };
	{
		Lock lock( &p_impl->m_mutex );

		AttrIterator global_attr     = p_impl->m_attrDefinitions.begin();
		AttrIterator global_attr_end = p_impl->m_attrDefinitions.end();
		int i = 0;
		for ( ; global_attr != global_attr_end; ++global_attr, ++i )
		{
			if ( global_attr->name == key ) 
			{
				AttributeValue test = { value, 0 };

				AttrValSetIterator found_value = global_attr->values.find( test );
				if ( found_value != global_attr->values.end() )
				{
					KeyValueID kv = { global_attr->id, found_value->id };

					if ( ! local_values ) {
						
					} else {
						test.id = found_value->id;
						local_values->insert( test );
					}

					return kv;
				} else {

				}
			}
		}

		// No global key value so make a new one and copy it back to the thread.

		ObjectAttribute newAttr;
		newAttr.name = key;
		newAttr.id   = p_impl->m_attrDefinitions.size();
		kv.attrNameIndex = newAttr.id;
		AttributeValue newValue = { value, 0 };
		newAttr.values.insert( newValue );

		// add to our global store
		p_impl->m_attrDefinitions.push_back( newAttr );

		/// we need to syncronise the global store to the thread store
		int syncIt = threadData->m_attrDefinitions.size()-1;
		threadData->m_attrDefinitions.resize( newAttr.id );
		for ( ; syncIt < newAttr.id; ++syncIt )
		{
			threadData->m_attrDefinitions[syncIt] = p_impl->m_attrDefinitions[ syncIt ];
		}

	} // lock released here

	return kv;
}

/// Get a unique integer ID from a set of key-value pair Ids.
/// The KeyValueID array must be pre-sorted in ascending order.
/// This function is thread safe.
AttrSetID AutomatteCollector::getAttrSetID( KeyValueID * attrs, int attrCount )
{
	// compute a hash of the combined list of input attributes
	uint64_t hash = ObjectAttributeSet::RecomputeHash( attrs, attrs + attrCount );

	ThreadData * threadData = ThreadData::Get();

	AttrSetIDMapIterator local_it = threadData->m_attrHashToIDMap.find( hash );
	if ( local_it != threadData->m_attrHashToIDMap.end() )
	{
		return local_it->second;
	}

	{
		// Not in our thread cache, look in our global store
		Lock lock( &p_impl->m_mutex );

		AttrSetIDMapIterator global_it = p_impl->m_attrHashToIDMap.find( hash );
		if ( global_it != p_impl->m_attrHashToIDMap.end() )
		{
			assert( hash == p_impl->m_attrSets[ global_it->second ].hash );

			threadData->m_attrHashToIDMap.insert( 
				AttrSetIDMap::value_type( hash, global_it->second ) 
			);
			return global_it->second;
		}

		// This is the first time we have requested this combination of attributes.
		// so make a new global entry and sync to the current thread data.

		ObjectAttributeSet objAttrSet( attrs, attrCount );
		assert( hash == objAttrSet.hash );

		objAttrSet.id = p_impl->m_attrSets.size();
		p_impl->m_attrSets.push_back( objAttrSet );

		threadData->m_attrSets.resize( p_impl->m_attrSets.size(), objAttrSet );
		threadData->m_attrSets[ objAttrSet.id ] = objAttrSet;

		p_impl->m_attrHashToIDMap.insert(
			AttrSetIDMap::value_type( hash, objAttrSet.id ) 
		);
		threadData->m_attrHashToIDMap.insert( 
			AttrSetIDMap::value_type( hash, objAttrSet.id ) 
		);

		return objAttrSet.id;
	}

	return AttrSetID();
}

/// Create a FilterLocation based on a raster space sub-pixel coordinate and a primary RayPathID.
FilterLocation * AutomatteCollector::createFilterLocation( RayPathID pathId, SubPixel raster, AttrSetID currId )
{
	ThreadData * threadData = ThreadData::Get();

	// There may be a pre-existing filter location for this pathId.
	// If so we will reuse it as it should be being used elsewhere.
	FilterLocation * loc = getPrimaryFilterLocation( pathId );
	if ( loc == NULL )
	{
		// TODO: Pull from memory pool allocator
		loc = new FilterLocation;
		loc->pixels = NULL;
		threadData->m_filterLocations[ pathId ] = loc;
	}

	loc->raster    = raster;
	loc->rayPathId = pathId;
	loc->tile      = getTile( raster );
	loc->rayTraversalType = kRayType_Primary;
	

	if ( loc->tile )
	{
		size_t px = (loc->tile->borderX*2+1);
		size_t py = (loc->tile->borderY*2+1);
		loc->pixelCount = px * py;

		// Create a local filtering neighbourhood for this FilterLocation
		if ( ! loc->pixels ) {
			// TODO : Pull From memory pool allocator
			loc->pixels = new AMT::FilterLocation::PixelLocation[ loc->pixelCount ];
		}

		float ox = (float)-(loc->tile->borderX+1);
		float oy = (float)-(loc->tile->borderY+1);
		size_t i = 0;
		for ( size_t x=0; x<px; px++, ox += 1.0f ) {
			for ( size_t y=0; y<py; py++, oy += 1.0f, i++ ) {
				// Create a short cut to the pixel layer index (i.e array offset)
				AMT::SubPixel filterRegion = { ox, oy };
				uint32_t pxlId = loc->tile->getSubPixelLayerIndex( filterRegion );
				loc->pixels[i].pixelLayerIndex = pxlId;	
				
				// Create short cuts to the correct offsets into the coverage array
				loc->pixels[i].coverage = loc->tile->getCoverage( pxlId, AMT::IdBufferTile::kPrimary, currId );
				loc->pixels[i].depth    = loc->tile->getDepth( pxlId, currId );

				// pre-compute the pixel filter.
				float filter = p_impl->m_frame.pixelFilter->filter( ox, oy );
				loc->pixels[i].filter = filter;
			}
		}
	}
	
	return loc;
}

/// Get a previously created primary FilterLocation based on a RayPathID.
inline FilterLocation * AutomatteCollector::getPrimaryFilterLocation( RayPathID pathId )
{
	ThreadData * threadData = ThreadData::Get();

	FilterLocactionMap::iterator it = threadData->m_filterLocations.find( pathId );
	if ( it != threadData->m_filterLocations.end() )
	{
		return it->second;
	}
	return NULL;
}

/// Collect an individual AttrSetID and sample weight into a FilterLocation 
/// attenuated via the current SubPixelFilter.
void AutomatteCollector::collectSample( 
	FilterLocation * loc, ///< The location in which to filter the id coverage sample.
	AttrSetID id,         ///< The id in which to filter.
	RayType rayType,      ///< the type of ray we are sampling
	float depth,          ///< the sample depth in camera space
	float weight          ///< An optional weight for the sample. Samples will be
						  /// attenuated by the SubPixelFilter that was passed
						  /// in via AutomatteCollector::initialise
)
{
	if ( ! (loc && loc->tile) || weight < 1e-08 )
		return;

	// if we align our tiles to the renderer buckets correctly then this
	// lock *should* have minimal to zero contention.
	Lock lock( &loc->tile->mutex );

	uint32_t * sampleCount = NULL;
	int rayTypeOffset = 0;
	AMT::IdBufferTile::CoverageChannel chan = AMT::IdBufferTile::kPrimary;
	switch ( rayType )
	{
		case kRayType_Primary      : sampleCount = &loc->primarySampleCount;
			break;
		case kRayType_Transmission : sampleCount = &loc->transmissionSampleCount;
			chan = AMT::IdBufferTile::kTransmission;
			rayTypeOffset++;
			break;
		case kRayType_Reflection   : sampleCount = &loc->reflectionSampleCount;
			rayTypeOffset++;
			chan = AMT::IdBufferTile::kReflection;
			if ( loc->tile->channelTypes && AMT::IdBufferTile::kTransmission )
				rayTypeOffset++;
			break;
	}

	if ( id != loc->currentId )
	{
		// The AttrSetID has changed since last sample on this FilterLocation
		// We need to refresh the pre-fetched grid of tile coverage pixel arrays.
		for ( int i=0; i<loc->pixelCount; i++ )
		{
			loc->pixels[i].coverage = loc->tile->getCoverage( loc->pixels[i].pixelLayerIndex, chan, id );
		}
	}


	// Accumulate the weight
	for ( int i=0; i<loc->pixelCount; i++ )
	{
		loc->pixels[i].coverage[rayTypeOffset] += weight * loc->pixels[i].filter;
	}

	// compute the depth range
	for ( int i=0; i<loc->pixelCount; i++ )
	{
		loc->pixels[i].depth->minDepth = std::min( loc->pixels[i].depth->minDepth, depth );
		loc->pixels[i].depth->maxDepth = std::max( loc->pixels[i].depth->maxDepth, depth );
	}
	sampleCount[0]++;
}


bool AutomatteCollector::saveSamplesToDisk( const std::string & filepath )
{
	return false;
}

AUTOMATTE_NS_END