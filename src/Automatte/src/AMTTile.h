#ifndef AMTTILE_H
#define AMTTILE_H

#include "AMTCommon.h"
#include <cstdlib>
#include <cmath>

AUTOMATTE_NS_BEGIN

/// Base Tile structure
///
/// Tiles can overlap by a border. This will result in wasted memory in a sense
/// but this is to allow for more uncontended parallel accumulation of samples
/// even with large filter radii, i.e when tiles are correctly algined to 
/// renderer buckets we can accumulate pixel samples at the full radius of a pixel 
/// filter without needing to lock the neighbouring tiles, leading to less thread
/// contention.
///
/// +----------------------+ ^
/// |< - - - -width- - - ->| |
/// |    +------------+    |  
/// |    |sx,sy       |    | h
/// |    |            |    | e
/// |    |            |    | i
/// |    |            |    | g
/// |    |            |    | h
/// |    |       ex,ey|    | t
/// |    +------------+    |  
/// |                      | |
/// +----------------------+ ^
///
struct Tile 
{
	int16_t       startPx;
	int16_t       startPy;
	int16_t       endPx;
	int16_t       endPy;
	int16_t       borderX; ///< an outer X border for the tile.
	int16_t       borderY; ///< an outer Y border for the tile.
	uint32_t      pixelArraySize;

	/// The full pixel height, including the border.
	inline uint16_t height() const { return (endPy - startPy) + borderY*2; }

	/// The full pixel width, including the border.
	inline uint16_t width() const  { return (endPx - startPx) + borderX*2; }

	/// Return a 1D array offset based on the tile local x,y coordinates.
	inline size_t arrayOffset( int x, int y ) 
	{
		assert( x >= -borderX );
		assert( y >= -borderY );
		assert( (x + borderX) <= width() );
		assert( (y + borderY) <= height() );
		return (x + borderX) + ((y + borderY) * width());
	}


	Tile() :
		startPx(0), startPy(0),
		endPx(0), endPy(0),
		borderX(0), borderY(0),
		pixelArraySize(0)
	{}

	Tile( 
		int16_t  _startPx,
		int16_t  _startPy,
		int16_t  _endPx,
		int16_t  _endPy,
		int16_t  _borderX,
		int16_t  _borderY
	) :
		startPx(_startPx), startPy(_startPy),
		endPx(_endPx), endPy(_endPy),
		borderX(_borderX), borderY(_borderY),
		pixelArraySize( width() * height() )
	{}
};


struct IdTile : Tile
{
	/// Coverage types
	enum RayType {
		kPrimary      = 1,
		kTransmission = 2,
		kReflection   = 4
	};

	// layout samples[y][x][rayType][layer]

	IdPixelSample * m_samples; 
};

/// Contains a voxel grid of integer Ids and float coverage masks for each id per-voxel.
///
/// The buffers will be resized when needed either by upgrading the integer
/// type to take larger id numbers, or increasing the number of idCapacity depth 
/// samples per-pixel.
struct IdBufferTile : Tile
{
	/// Integer storage class for the ID
	enum DataType {
		kUint8 = 1,
		kUint16 = 2,
		kUint32 = 4
	};

	/// Coverage types
	enum CoverageChannel {
		kPrimary      = 1,
		kTransmission = 2,
		kReflection   = 4
	};

	union {
		uint8_t  * id_asUint8;  ///< an array of idCapacity up to 
		uint16_t * id_asUint16;
		uint32_t * id_asUint32;
	};
	float     * coverage;     ///< a pixel array of primary and/or secondary coverage values.
	Depth     * depth;        ///< a pixel array of min and max sample depths.
	uint32_t  * sampleCounts; ///< a pixel array of primary and/or secondary sample counts
	uint16_t    idCapacity;   ///< the max number of ids able to be stored per-pixel.
	DataType    dataType;     ///< the current integer storage type for the ids
	uint8_t     channelTypes; ///< a bit combination of the ray channel types for this tile.
	uint8_t     numChannels;  ///< the number of ray channels 1, 2 or 3.
	Mutex       mutex;        ///< A tile specific Mutex lock.

	IdBufferTile() :
		Tile(),
		id_asUint8(NULL),
		coverage(NULL),
		idCapacity(0),
		dataType(kUint8),
		channelTypes(kPrimary),
		numChannels(1)
	{}

	/// Return the approximate memory usage for this Tile.
	inline size_t memory() const
	{
		size_t voxels = pixelArraySize * idCapacity;
		return sizeof(IdBufferTile) + 
				(voxels * dataType) +                      // id
				(voxels * sizeof(Depth)) +                 // depth
				(voxels * sizeof(float) * numChannels) +   // coverage
				(pixelArraySize * sizeof(uint32_t) * numChannels); // sampleCounts
	}

	/// Initialise the geometry and type of this Tile.
	void init( 
		int startPx, int startPy, int endPx, int endPy, 
		int borderX, int borderY, 
		int initialIdCapacity = 2, ///< two layers per-pixel pre-allocated
		DataType initialDataType = kUint8,
		int initChannelTypes = kPrimary 
	);
	
	/// Upgrade the id integer type for this Tile so larger id integers can be stored.
	void upgradeIdInteger();

	/// Add more idCapacity layers to this tile
	void resizeIdLayers( uint16_t newIdCapacity );

	/// Free the buffer memory of this Tile.
	inline void finalise()
	{
		if ( coverage )
		{
			free( coverage );
			coverage = NULL;
		}
		if ( depth )
		{
			delete [] depth;
			depth = NULL;
		}
		if ( sampleCounts )
		{
			free( sampleCounts );
			sampleCounts = NULL;
		}
		if ( id_asUint8 )
		{
			free( id_asUint8 );
			id_asUint8 = NULL;
		}
	}

	/// get the offset into the id or coverage array at a specific pixel.
	inline uint32_t getSubPixelLayerIndex( SubPixel pixel, int32_t layer = 0 )
	{
		int tileRelativeX = std::floor(pixel.x) - this->startPx;
		int tileRelativeY = std::floor(pixel.y) - this->startPy;

		return (arrayOffset(tileRelativeX,tileRelativeY) * idCapacity) + layer;
	}

	/// Get an AttrSetID for the a pixel layer index.
	/// @param pixelLayerIndex an index returned from IdBufferTile::getSubPixelLayerIndex
	inline AttrSetID getId( uint32_t pixelLayerIndex ) 
	{
		assert( pixelLayerIndex < width() * height() * idCapacity );

		switch ( dataType ) {
			case kUint8  : return (AttrSetID)id_asUint8[ pixelLayerIndex ];
			case kUint16 : return (AttrSetID)id_asUint16[ pixelLayerIndex ];
			case kUint32 : return (AttrSetID)id_asUint32[ pixelLayerIndex ];
		}
		return 0;
	}

	/// Set the AttrSetID at a specific pixel layer index
	inline void setId( uint32_t pixelLayerIndex, AttrSetID id ) 
	{
		assert( pixelLayerIndex < pixelArraySize * idCapacity );

		switch ( dataType ) {
			case kUint8  : { 
				assert( id <= std::numeric_limits<uint8_t>::max() );
				id_asUint8[ pixelLayerIndex ] = (uint8_t)id;
				break;
			}
			case kUint16 : {
				assert( id <= std::numeric_limits<uint16_t>::max() );
				id_asUint16[ pixelLayerIndex ] = (uint16_t)id;
				break;
			}
			case kUint32 : id_asUint32[ pixelLayerIndex ] = (uint32_t)id;
		}
	}

	/// Return the coverage array for a particular pixel layer matching the passed
	/// AttrSetID in tile space.
	///
	/// The return float array will be either 1, 2 or 3 elements long depending on 
	/// whether transmission and or reflection coverage is requested.
	///
	/// pixelLayerIndex is assumed to be at the start of a pixel layer.
	/// If the Id has not been sampled before, the next available id slot is created.
	/// If this returns NULL, the idCapacity needs to be resized.
	///
	/// coverage array 4D array layout == coverage[idLayer][rayType][y][x]
	/// +-----+-----+-----+....+-----+-----+-----+....+-----+-----+-----+
	/// |X1,Y1|X2,Y1|X3,Y1|....|X1,Y1|X2,Y1|X3,Y1|....|X1,Y1|X2,Y1|X3,Y1|
	/// | id1 | id1 | id1 |....| id1 | id1 | id1 |....| id1 | id1 | id1 |
	/// +-----+-----+-----+....+-----+-----+-----+....+-----+-----+-----+
	/// |  P  |  P  |  P  |....|  T  |  T  |  T  |....|  R  |  R  |  R  |
	/// +-----+-----+-----+....+-----+-----+-----+....+-----+-----+-----+
	inline float * getCoverage( uint32_t pixelLayerIndex, CoverageChannel channel, AttrSetID id )
	{
		// Search for the id coverage sample on the requested pixel.
		for ( int layerOffset=0; layerOffset < idCapacity; layerOffset++ ) 
		{
			AttrSetID curr_id = getId( pixelLayerIndex + layerOffset);

			if ( curr_id == 0 || curr_id == id ) 
			{
				// id == 0 means the background layer so it's free to overwrite
				if ( curr_id == 0 ) { 
					setId( pixelLayerIndex + layerOffset, id );
				}

				int chanOffset = 0;
				if ( channel == kTransmission ) {
					chanOffset += 1;
				} else 
				if ( channel == kReflection ) { 
					chanOffset += channelTypes & kTransmission ? 2 : 1;
				}

				int xyCoord = (pixelLayerIndex / idCapacity);
				return &coverage[ (idCapacity * (chanOffset + layerOffset)) + xyCoord ];
			} 
		}

		// If we got here we need to resize the idCapacity to fit more objects
		// in the same pixel.
		return NULL;
	}

	/// Get the depth range for the current Id
	inline Depth * getDepth( uint32_t pixelLayerIndex, AttrSetID id )
	{

		// Search for the id coverage sample on the requested pixel.
		for ( int layerOffset=0; layerOffset < idCapacity; layerOffset++ ) 
		{
			AttrSetID curr_id = getId( pixelLayerIndex + layerOffset);

			if ( curr_id == 0 || curr_id == id ) 
			{
				// id == 0 means the background layer so it's free to overwrite
				if ( curr_id == 0 ) { 
					setId( pixelLayerIndex + layerOffset, id );
				}

				int xyCoord = (pixelLayerIndex / idCapacity);
				return &depth[ (layerOffset * idCapacity) + xyCoord ];
			} 
		}

		// If we got here we need to resize the idCapacity to fit more objects
		// in the same pixel.
		return NULL;
	}

	/// Get the per-pixel sample counts for primary and/or secondary ray types for
	/// the pixelLayerIndex.
	inline uint32_t * getSampleCounts( uint32_t pixelLayerIndex, CoverageChannel channel )
	{
		int chanOffset = 0;
		if ( channel == kTransmission ) {
			chanOffset += 1;
		} else 
		if ( channel == kReflection ) { 
			chanOffset += channelTypes & kTransmission ? 2 : 1;
		}
		int xyCoord = (pixelLayerIndex / idCapacity);
		return &sampleCounts[ (chanOffset * pixelArraySize) + xyCoord ];
	}
};


inline void IdBufferTile::upgradeIdInteger()
{
	if ( dataType == kUint32 )
		return;

	Lock lock( &mutex );

	DataType nextDataType = dataType == kUint16 ? kUint32 : kUint16;
	
	uint32_t voxels = pixelArraySize * idCapacity;
	uint8_t * nextIdBuffer = static_cast<uint8_t*>( malloc( voxels * nextDataType ) );

	/// upgrade the existing layer data.
	switch( nextDataType ) 
	{
		case kUint16: {
			uint16_t * next_asUint16 = reinterpret_cast<uint16_t*>( nextIdBuffer );
			for ( uint32_t i=0; i<voxels; i++ )
			{
				next_asUint16[ i ] = (uint16_t) id_asUint8[ i ];
			}
			break;
		}
		case kUint32: {
			uint32_t * next_asUint32 = reinterpret_cast<uint32_t*>( nextIdBuffer );
			for ( uint32_t i=0; i<voxels; i++ )
			{
				next_asUint32[ i ] = (uint32_t) id_asUint16[ i ];
			}
		}
		default: break;
	}

	free( id_asUint8 );
	id_asUint8 = nextIdBuffer;
}

inline void IdBufferTile::init( 
	int startPx, int startPy, int endPx, int endPy, 
	int borderX, int borderY, 
	int initialIdCapacity,
	DataType initialDataType,
	int initChannelTypes
)
{
	finalise();

	Lock lock( &mutex );

	this->startPx = startPx;
	this->startPy = startPy;
	this->endPx   = endPx;
	this->endPy   = endPy;
	this->borderX = borderX;
	this->borderY = borderY;
	this->pixelArraySize = width() * height();

	idCapacity   = std::max( 1, initialIdCapacity );
	dataType     = initialDataType;
	channelTypes = initChannelTypes;
	numChannels  = 0;
	if ( channelTypes & kPrimary ) 
		numChannels += 1;
	if ( channelTypes & kTransmission ) 
		numChannels += 1;
	if ( channelTypes & kReflection ) 
		numChannels += 1;

	uint32_t voxels = pixelArraySize * idCapacity;

	id_asUint8 = static_cast<uint8_t*>( calloc( voxels * dataType, 0 ) );
	coverage   = static_cast<float*>( calloc( voxels * sizeof(float) * numChannels, 0 ) );
	depth      = new Depth[ voxels * numChannels ];
	sampleCounts = static_cast<uint32_t*>( calloc( pixelArraySize * sizeof(uint32_t) * numChannels, 0 ) );
}

inline void IdBufferTile::resizeIdLayers( uint16_t newIdCapacity )
{
	if ( newIdCapacity <= idCapacity )
		return;

	assert( (coverage && id_asUint8) && "Tile has not been initialised!" );

	Lock lock(&mutex );

	uint32_t voxels = pixelArraySize * newIdCapacity;

	uint8_t * nextIdBuffer = static_cast<uint8_t*>( calloc( voxels * dataType, 0 ) );
	float * nextCoverage   = static_cast<float*>( calloc( voxels * sizeof(float) * numChannels, 0 ) );
	Depth * nextDepth      = new Depth[ voxels * numChannels ];

	// The id array layout is contiguous in id layer stips as we commonly
	// traverse through each layer for a specific pixel. i.e:
	// 		id[y][x][layer]
	for ( uint32_t i=0; i<pixelArraySize; i++ )
	{
		// Copy the previous layer ids over
		std::memcpy( 
			nextIdBuffer + (i * dataType * newIdCapacity), 
			id_asUint8   + (i * dataType * idCapacity), 
			(idCapacity * dataType) 
		);
	}

	// As we commonly traverse multiple pixel values for a specific id and channel Type,
	// the coverage array stores the numChannels coverage for each id layer in 
	// contiguous 2d pixel arrays to hopefully increase memory cache locality for
	// coverage values of the same Id and channel type. i.e:
	// 		coverage[id][channelType][y][x]
	std::memcpy( 
		nextCoverage, 
		coverage, 
		pixelArraySize * numChannels * idCapacity * sizeof(float)
	);

	free( id_asUint8 );
	id_asUint8 = nextIdBuffer;

	free( coverage );
	coverage = nextCoverage;

	delete [] depth;
	depth = nextDepth;
}

AUTOMATTE_NS_END


#endif
