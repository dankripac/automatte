# Automatte #

**work in progress**

An automatic ID matte library with client plug-ins for Prman RIS and Nuke.

Inspired by Psyop's Cryptomatte 2015 Siggraph presentation it intends to work with pre-filtered pixel samples to accumulate ID coverage in depth sorted layers.

Where I'm aiming to differ from cryptomatte is:

* Use a manifest based ID info accumulation (rather than just object path hashing) that supports arbitrary amounts of attribute data per-primitive.
    * Arbitrary attributes per-primitive would commonly include:
        * Object path
        * Material name
        * Material Look file publish ID
        * Geo Asset publishing ID
        * (anything else deemed useful to filter for inside Nuke)
        * All attribute values would coalesce to a single integer ID value per-primitive.
* Storing an aribrary amount of per-pixel ID layers rather than needing to define your maximum amount of layers (i.e 3,6,9 etc).
* Try to use deep exrs to store ID layers but not using conventional deep samples - more to use the deep exr as a vehicle to store depth sorted transparent ID values.
* Attempt to support the additional storage of transmitted specular ray paths (i.e refraction) per-ID sample so that mattes can be pulled through refracted materials.
* Also experiment with reflected samples (but this may prove too expensive?)
* Look at storing the min-max depth range of all ID samples so that it can be used to aid in reconstructing a deep image in Nuke to aid in depth compositing.